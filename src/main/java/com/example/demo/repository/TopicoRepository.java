package com.example.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.modelo.Topico;

public interface TopicoRepository extends JpaRepository<Topico, Long>{

	Page<Topico> findByCursoNome(String nome, Pageable paginacao);
	
	@Query("SELECT t FROM Topico t WHERE t.curso.nome = :nome")
	List<Topico> buscarPeloNomeDoCurso(String nome);
	
}
