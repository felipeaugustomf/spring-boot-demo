package com.example.demo.config.swagger;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.modelo.Usuario;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ParameterType;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfigurations {

	@Bean
	public Docket demoApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.example.demo"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo())
				.ignoredParameterTypes(Usuario.class)
				.globalRequestParameters(Arrays.asList(
						new RequestParameterBuilder()
                        	.name("Authorization")
			                .description("Header para Token JWT")
			                .in(ParameterType.HEADER)
			                .required(false)
			                .build()))		
				;
	}
	
	private ApiInfo apiInfo() {
		return new ApiInfo(
				"RESTful API With Spring Boot", 
				"Api de demonstração usando Spring Boot",
				"v1",
				"Terms Of Service Url",
				new Contact("Felipe Augusto", null, "felipeaugustomf@gmail.com"),
				"License of API", "License of URL", Collections.emptyList());
	}

}
